﻿using Microsoft.AspNetCore.SignalR.Client;
using System;

namespace SignalRTestConsole
{
    static class Program
    {
        private static void Main(string[] args)
        {
            HubConnection connection = new HubConnectionBuilder().WithUrl("http://localhost:5000/chathub").Build();

            connection.StartAsync().Wait();

            connection.On<string, string>("ReceiveMessage", (user, message) => { Console.WriteLine($"{user} said {message}"); });

            while (true)
            {
                string answer = Console.ReadLine();
                connection.InvokeAsync("SendMessage", 1, answer);
            }
        }
    }
}