﻿using Microsoft.AspNetCore.SignalR.Client;
using System;

namespace SignalRTestConsoleSend
{
    static class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            HubConnection connection = new HubConnectionBuilder().WithUrl("http://localhost:5000/chathub").Build();

            connection.StartAsync().Wait();
            Console.WriteLine("Connected!");

            while (true)
            {
                string answer = Console.ReadLine();
                connection.InvokeAsync("SendMessage", "ME", answer);
            }
        }
    }
}