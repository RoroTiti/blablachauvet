﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlaBlaChauvet.MobileAppService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "MDL")]

    public class MDLController : ControllerBase
    {

        [HttpGet]
        public ActionResult Bonjour()
        {
            return Ok("bonjour MDL");
        }

    }
}