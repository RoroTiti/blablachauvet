﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlaBlaChauvet.MobileAppService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "VS")]

    public class VieScolaireController : ControllerBase
    {

        [HttpGet]
        public ActionResult Bonjour()
        {
            return Ok("bonjour vie scolaire");
        }

    }
}