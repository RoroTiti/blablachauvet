﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BlaBlaChauvet.MobileAppService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        [HttpPost("token")]
        public ActionResult GetToken(string login, string password)
        {
            string leMotdePasse1, leMotdePasse2, leMotdePasse3;
            bool VS, MDL, CVL;
            VS = false;
            MDL = false;
            CVL = false;

            leMotdePasse1 = "6ac557814b7a5cefec36a5bff74b957246515aa190c76b2689ae482d5ef467c4";
            leMotdePasse2 = "1dff19d92a0fd8825ae7a93e9214a6a0c179b61321b227f50315cccc62a92952";
            leMotdePasse3 = "841813ddcbfe4a857c2d6b416c20919a53b936a108db0c4ee8701387d1b56ed9";

            password = Helpers.ComputeSha256Hash(password);
            Debug.WriteLine(password);
            switch (login)
            {
                case "MDL":
                    if (password == leMotdePasse1)
                    {
                        MDL = true;
                    }
                    else
                    {
                        return Forbid();
                    }

                    break;

                case "VS":
                    if (password == leMotdePasse2)
                    {
                        VS = true;
                    }
                    else
                    {
                        return Forbid();
                    }

                    break;

                case "CVL":
                    if (password == leMotdePasse3)
                    {
                        CVL = true;
                    }
                    else
                    {
                        return Forbid();
                    }

                    break;

                default:
                    return Forbid();
            }


            //security key
            string securityKey = "this_is_our_supper_long_security_key_for_token_validation_project_2018_09_07$smesk.in";
            //symmetric security key
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

            //signing credentials
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

            //add claims
            var claims = new List<Claim>();
            if (VS)
            {
                claims.Add(new Claim(ClaimTypes.Role, "VS"));
            }
            else if (MDL)
            {
                claims.Add(new Claim(ClaimTypes.Role, "MDL"));
            }
            else if (CVL)
            {
                claims.Add(new Claim(ClaimTypes.Role, "CVL"));
            }

            //create token
            var token = new JwtSecurityToken(
                issuer: "smesk.in",
                audience: "users",
                expires: DateTime.Now.AddHours(2),
                signingCredentials: signingCredentials,
                claims: claims
            );

            //return token
            return Ok(new JwtSecurityTokenHandler().WriteToken(token));
        }
    }
}