﻿using BlaBlaChauvet.MobileAppService.Models.EntityFramework;
using BlaBlaChauvet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace BlaBlaChauvet.MobileAppService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserManagementController : ControllerBase
    {
        [HttpPost("login")]
        public ActionResult GetToken(string email, string password)
        {
            //Connexion à la bdd
            using (var context = new DataBaseContext())
            {
                //On récupere l'utilisateur qui correspond à notre email
                Utilisateur LUtilisateur = context.Utilisateur.Single(x => x.Mail.Contains(email));

                Debug.WriteLine($"l'email saisit est {email}");
                Debug.WriteLine($"le mot de passe saisit est {password}");
                Debug.WriteLine($"le mot de passe de la bdd est {LUtilisateur.Mdp}");

                //Si on ne trouve aucun utilisateur ou que notre mot de passe ne correspond pas, alors on quitte
                if (LUtilisateur == null || LUtilisateur.Mdp != password)
                {
                    return Forbid();
                }
                else
                {
                    //Sinon, on recupere le type utilisateur de l'utilisateur
                    TypeUtilisateur LeTypeUtilisateur = context.TypeUtilisateur.SingleOrDefault(x => x.IdTypeUtilisateur == LUtilisateur.IdTypeUtilisateur);

                    //On défini la clé secrete
                    string securityKey = "this_is_our_supper_long_security_key_for_token_validation_project_2018_09_07$smesk.in";

                    var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey));

                    //signing credentials
                    var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);

                    //add claims
                    var claims = new List<Claim>();

                    claims.Add(new Claim(ClaimTypes.Role, LeTypeUtilisateur.Libelle));

                    int idUser = LUtilisateur.IdUtilisateur * 2;

                    claims.Add(new Claim(ClaimTypes.NameIdentifier, idUser.ToString()));


                    //create token
                    var token = new JwtSecurityToken(
                            issuer: "smesk.in",
                            audience: "users",
                            expires: DateTime.Now.AddHours(2),
                            signingCredentials: signingCredentials,
                            claims: claims
                        );

                    //return token
                    return Ok(new JwtSecurityTokenHandler().WriteToken(token));
                }
            };
        }

        [HttpPost("me")]
        public ActionResult GetUser()
        {
            //On recupere l'id de l'utilisateur dans le claim de notre token
            string idUser = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            int idUtilisateur = int.Parse(idUser) / 2;

            //On se connecte a la bdd
            using (var context = new DataBaseContext())
            {
                //On récupere l'utilisateur qui correspond à notre email
                Utilisateur LUtilisateur = context.Utilisateur.SingleOrDefault(x => x.IdUtilisateur == idUtilisateur);

                //On créer un tableau json
                JArray array = new JArray();

                //On recupere toutes les valeurs qui nous interessent
                JValue Nom = new JValue(LUtilisateur.Nom);
                JValue Prenom = new JValue(LUtilisateur.Prenom);
                JValue Mail = new JValue(LUtilisateur.Mail);

                Classe LaCLasse = context.Classe.SingleOrDefault(x => x.IdClasse == LUtilisateur.IdClasse);
                TypeUtilisateur LeTypeUtilisateur = context.TypeUtilisateur.SingleOrDefault(x => x.IdTypeUtilisateur == LUtilisateur.IdTypeUtilisateur);

                JValue LibelleClasse = new JValue(LaCLasse.Libelle);
                JValue LibelleTypeUtilisateur = new JValue(LeTypeUtilisateur.Libelle);
                JValue Couleur = new JValue(LeTypeUtilisateur.Couleur);

                //On ajoute nos valeurs dans notre tableau
                array.Add(Nom);
                array.Add(Prenom);
                array.Add(Mail);
                array.Add(LibelleClasse);
                array.Add(LibelleTypeUtilisateur);
                array.Add(Couleur);

                string json = array.ToString();

                //On retourne le tableau
                return Ok(json);
            }
        }
    }
}