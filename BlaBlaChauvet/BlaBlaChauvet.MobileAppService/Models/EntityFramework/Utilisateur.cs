﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Utilisateur
    {
        public Utilisateur()
        {
            Chat = new HashSet<Chat>();
            PersoIdUtilisateur1Navigation = new HashSet<Perso>();
            PersoIdUtilisateurNavigation = new HashSet<Perso>();
        }

        public int IdUtilisateur { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
        public string Mail { get; set; }
        public string Mdp { get; set; }
        public int? IdClasse { get; set; }
        public int IdTypeUtilisateur { get; set; }

        public virtual Classe IdClasseNavigation { get; set; }
        public virtual TypeUtilisateur IdTypeUtilisateurNavigation { get; set; }
        public virtual ICollection<Chat> Chat { get; set; }
        public virtual ICollection<Perso> PersoIdUtilisateur1Navigation { get; set; }
        public virtual ICollection<Perso> PersoIdUtilisateurNavigation { get; set; }
    }
}
