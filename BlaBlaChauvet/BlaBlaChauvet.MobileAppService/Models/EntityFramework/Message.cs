﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Message
    {
        public Message()
        {
            Fichier = new HashSet<Fichier>();
            Posseder = new HashSet<Posseder>();
        }

        public int IdMessage { get; set; }
        public string Message1 { get; set; }
        public DateTime? DateHeure { get; set; }

        public virtual Chat Chat { get; set; }
        public virtual FilActu FilActu { get; set; }
        public virtual Perso Perso { get; set; }
        public virtual ICollection<Fichier> Fichier { get; set; }
        public virtual ICollection<Posseder> Posseder { get; set; }
    }
}
