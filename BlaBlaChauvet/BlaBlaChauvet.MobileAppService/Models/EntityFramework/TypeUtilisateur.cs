﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class TypeUtilisateur
    {
        public TypeUtilisateur()
        {
            FilActu = new HashSet<FilActu>();
            Utilisateur = new HashSet<Utilisateur>();
        }

        public int IdTypeUtilisateur { get; set; }
        public string Libelle { get; set; }
        public string Couleur { get; set; }

        public virtual ICollection<FilActu> FilActu { get; set; }
        public virtual ICollection<Utilisateur> Utilisateur { get; set; }
    }
}
