﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Posseder
    {
        public int IdSalons { get; set; }
        public int IdMessage { get; set; }

        public virtual Message IdMessageNavigation { get; set; }
        public virtual Salon IdSalonsNavigation { get; set; }
    }
}
