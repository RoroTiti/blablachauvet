﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Perso
    {
        public int IdMessage { get; set; }
        public int IdUtilisateur { get; set; }
        public int IdUtilisateur1 { get; set; }

        public virtual Message IdMessageNavigation { get; set; }
        public virtual Utilisateur IdUtilisateur1Navigation { get; set; }
        public virtual Utilisateur IdUtilisateurNavigation { get; set; }
    }
}
