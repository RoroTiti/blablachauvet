﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Chat
    {
        public int IdMessage { get; set; }
        public int IdUtilisateur { get; set; }

        public virtual Message IdMessageNavigation { get; set; }
        public virtual Utilisateur IdUtilisateurNavigation { get; set; }
    }
}
