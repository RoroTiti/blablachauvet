﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Classe
    {
        public Classe()
        {
            Utilisateur = new HashSet<Utilisateur>();
        }

        public int IdClasse { get; set; }
        public string Libelle { get; set; }

        public virtual ICollection<Utilisateur> Utilisateur { get; set; }
    }
}
