﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Fichier
    {
        public int IdFichier { get; set; }
        public string NomFichier { get; set; }
        public int IdMessage { get; set; }

        public virtual Message IdMessageNavigation { get; set; }
    }
}
