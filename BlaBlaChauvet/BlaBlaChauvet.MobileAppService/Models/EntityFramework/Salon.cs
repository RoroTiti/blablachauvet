﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class Salon
    {
        public Salon()
        {
            Posseder = new HashSet<Posseder>();
        }

        public int IdSalons { get; set; }
        public string Libelle { get; set; }

        public virtual ICollection<Posseder> Posseder { get; set; }
    }
}
