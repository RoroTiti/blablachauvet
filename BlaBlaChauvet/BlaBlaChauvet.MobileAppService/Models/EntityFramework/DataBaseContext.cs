﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class DataBaseContext : DbContext
    {
        public DataBaseContext()
        {
        }

        public DataBaseContext(DbContextOptions<DataBaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Chat> Chat { get; set; }
        public virtual DbSet<Classe> Classe { get; set; }
        public virtual DbSet<Fichier> Fichier { get; set; }
        public virtual DbSet<FilActu> FilActu { get; set; }
        public virtual DbSet<Message> Message { get; set; }
        public virtual DbSet<Perso> Perso { get; set; }
        public virtual DbSet<Posseder> Posseder { get; set; }
        public virtual DbSet<Salon> Salon { get; set; }
        public virtual DbSet<TypeUtilisateur> TypeUtilisateur { get; set; }
        public virtual DbSet<Utilisateur> Utilisateur { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=romaric-thibault.ddns.net;Database=BlaBlaChauvet;persist security info=True;user id=BlaBlaChauvet; password=BlaBlaChauvet86");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Chat>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK__Chat__460F3CF4E651AD35");

                entity.Property(e => e.IdMessage)
                    .HasColumnName("id_message")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdUtilisateur).HasColumnName("id_utilisateur");

                entity.HasOne(d => d.IdMessageNavigation)
                    .WithOne(p => p.Chat)
                    .HasForeignKey<Chat>(d => d.IdMessage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Chat__id_message__37A5467C");

                entity.HasOne(d => d.IdUtilisateurNavigation)
                    .WithMany(p => p.Chat)
                    .HasForeignKey(d => d.IdUtilisateur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Chat__id_utilisa__38996AB5");
            });

            modelBuilder.Entity<Classe>(entity =>
            {
                entity.HasKey(e => e.IdClasse)
                    .HasName("PK__Classe__29F53FA672614161");

                entity.Property(e => e.IdClasse).HasColumnName("id_classe");

                entity.Property(e => e.Libelle).HasColumnName("libelle");
            });

            modelBuilder.Entity<Fichier>(entity =>
            {
                entity.HasKey(e => e.IdFichier)
                    .HasName("PK__Fichier__EB55C93B81A3B2A9");

                entity.Property(e => e.IdFichier).HasColumnName("id_fichier");

                entity.Property(e => e.IdMessage).HasColumnName("id_message");

                entity.Property(e => e.NomFichier)
                    .IsRequired()
                    .HasColumnName("nom_fichier");

                entity.HasOne(d => d.IdMessageNavigation)
                    .WithMany(p => p.Fichier)
                    .HasForeignKey(d => d.IdMessage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Fichier__id_mess__2D27B809");
            });

            modelBuilder.Entity<FilActu>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK__Fil_actu__460F3CF4F874CA71");

                entity.ToTable("Fil_actu");

                entity.Property(e => e.IdMessage)
                    .HasColumnName("id_message")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdTypeUtilisateur).HasColumnName("id_type_utilisateur");

                entity.Property(e => e.Titre).HasColumnName("titre");

                entity.HasOne(d => d.IdMessageNavigation)
                    .WithOne(p => p.FilActu)
                    .HasForeignKey<FilActu>(d => d.IdMessage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Fil_actu__id_mes__300424B4");

                entity.HasOne(d => d.IdTypeUtilisateurNavigation)
                    .WithMany(p => p.FilActu)
                    .HasForeignKey(d => d.IdTypeUtilisateur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Fil_actu__id_typ__30F848ED");
            });

            modelBuilder.Entity<Message>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK__Message__460F3CF4C4040BFD");

                entity.Property(e => e.IdMessage).HasColumnName("id_message");

                entity.Property(e => e.DateHeure)
                    .HasColumnName("date_heure")
                    .HasColumnType("datetime");

                entity.Property(e => e.Message1).HasColumnName("message");
            });

            modelBuilder.Entity<Perso>(entity =>
            {
                entity.HasKey(e => e.IdMessage)
                    .HasName("PK__perso__460F3CF41600C561");

                entity.ToTable("perso");

                entity.Property(e => e.IdMessage)
                    .HasColumnName("id_message")
                    .ValueGeneratedNever();

                entity.Property(e => e.IdUtilisateur).HasColumnName("id_utilisateur");

                entity.Property(e => e.IdUtilisateur1).HasColumnName("id_utilisateur_1");

                entity.HasOne(d => d.IdMessageNavigation)
                    .WithOne(p => p.Perso)
                    .HasForeignKey<Perso>(d => d.IdMessage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__perso__id_messag__3B75D760");

                entity.HasOne(d => d.IdUtilisateurNavigation)
                    .WithMany(p => p.PersoIdUtilisateurNavigation)
                    .HasForeignKey(d => d.IdUtilisateur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__perso__id_utilis__3C69FB99");

                entity.HasOne(d => d.IdUtilisateur1Navigation)
                    .WithMany(p => p.PersoIdUtilisateur1Navigation)
                    .HasForeignKey(d => d.IdUtilisateur1)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__perso__id_utilis__3D5E1FD2");
            });

            modelBuilder.Entity<Posseder>(entity =>
            {
                entity.HasKey(e => new { e.IdSalons, e.IdMessage })
                    .HasName("PK__Posseder__A1119E8A157F2D2B");

                entity.Property(e => e.IdSalons).HasColumnName("id_Salons");

                entity.Property(e => e.IdMessage).HasColumnName("id_message");

                entity.HasOne(d => d.IdMessageNavigation)
                    .WithMany(p => p.Posseder)
                    .HasForeignKey(d => d.IdMessage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Posseder__id_mes__412EB0B6");

                entity.HasOne(d => d.IdSalonsNavigation)
                    .WithMany(p => p.Posseder)
                    .HasForeignKey(d => d.IdSalons)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Posseder__id_Sal__403A8C7D");
            });

            modelBuilder.Entity<Salon>(entity =>
            {
                entity.HasKey(e => e.IdSalons)
                    .HasName("PK__Salon__F5716D45EF6C8C00");

                entity.Property(e => e.IdSalons).HasColumnName("id_Salons");

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasColumnName("libelle");
            });

            modelBuilder.Entity<TypeUtilisateur>(entity =>
            {
                entity.HasKey(e => e.IdTypeUtilisateur)
                    .HasName("PK__Type_uti__8C035B79A29934AD");

                entity.ToTable("Type_utilisateur");

                entity.Property(e => e.IdTypeUtilisateur).HasColumnName("id_type_utilisateur");

                entity.Property(e => e.Couleur)
                    .HasColumnName("couleur")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Libelle)
                    .IsRequired()
                    .HasColumnName("libelle");
            });

            modelBuilder.Entity<Utilisateur>(entity =>
            {
                entity.HasKey(e => e.IdUtilisateur)
                    .HasName("PK__Utilisat__1A4FA5B8B0399A6F");

                entity.Property(e => e.IdUtilisateur).HasColumnName("id_utilisateur");

                entity.Property(e => e.IdClasse).HasColumnName("id_classe");

                entity.Property(e => e.IdTypeUtilisateur).HasColumnName("id_type_utilisateur");

                entity.Property(e => e.Mail).HasColumnName("mail");

                entity.Property(e => e.Mdp).HasColumnName("mdp");

                entity.Property(e => e.Nom)
                    .IsRequired()
                    .HasColumnName("nom");

                entity.Property(e => e.Prenom)
                    .IsRequired()
                    .HasColumnName("prenom");

                entity.HasOne(d => d.IdClasseNavigation)
                    .WithMany(p => p.Utilisateur)
                    .HasForeignKey(d => d.IdClasse)
                    .HasConstraintName("FK__Utilisate__id_cl__33D4B598");

                entity.HasOne(d => d.IdTypeUtilisateurNavigation)
                    .WithMany(p => p.Utilisateur)
                    .HasForeignKey(d => d.IdTypeUtilisateur)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Utilisate__id_ty__34C8D9D1");
            });
        }
    }
}
