﻿using System;
using System.Collections.Generic;

namespace BlaBlaChauvet.MobileAppService.Models.EntityFramework
{
    public partial class FilActu
    {
        public int IdMessage { get; set; }
        public string Titre { get; set; }
        public int IdTypeUtilisateur { get; set; }

        public virtual Message IdMessageNavigation { get; set; }
        public virtual TypeUtilisateur IdTypeUtilisateurNavigation { get; set; }
    }
}
