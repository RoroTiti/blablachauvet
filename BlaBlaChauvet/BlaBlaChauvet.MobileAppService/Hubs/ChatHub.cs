﻿using BlaBlaChauvet.MobileAppService.Models.EntityFramework;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlaBlaChauvet.MobileAppService.Hubs
{
    public class ChatHub : Hub
    {
        public async Task SendMessage(int idUtilisateur, string answer)
        {
            using (var context = new DataBaseContext())
            {
                Utilisateur LUtilisateur = context.Utilisateur.SingleOrDefault(x => x.IdUtilisateur == idUtilisateur);

                if (LUtilisateur == null)
                {
                    return;
                }
            };

            List<Task> tasks = new List<Task>();

            Task Task1 = new Task(async () =>
            {
                await Clients.All.SendAsync("ReceiveMessage", idUtilisateur, answer);
            });

            Task Task2 = new Task(async () =>
            {
                using (var context = new DataBaseContext())
                {
                    Message leMessage = new Message { Message1 = answer, DateHeure = DateTime.Now };
                    context.Message.Add(leMessage);
                    await context.SaveChangesAsync();
                    Chat leChat = new Chat { IdMessage = leMessage.IdMessage, IdUtilisateur = idUtilisateur };
                    context.Chat.Add(leChat);
                    await context.SaveChangesAsync();

                };
            });

            tasks.Add(Task1);
            tasks.Add(Task2);

            Parallel.ForEach(tasks, task => task.Start());
            Task.WaitAll(tasks.ToArray());
        }
    }
}